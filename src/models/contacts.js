const mongoose = require('mongoose');
const { Schema } = mongoose;

const ContacsSchema = new Schema({
  nombre: { type: String, required: true },
  email: { type: String, required: true },
  telefono: { type: String, required: true },
  tipocontact: { type: String, required: false }
});

module.exports = mongoose.model('Contact', ContacsSchema);
