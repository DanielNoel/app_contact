const express = require('express');
const router = express.Router();

// Task Model
const Contact = require('../models/contacts');

// GET all Contacs
router.get('/', async (req, res) => {
  const contacts = await Contact.find();
  res.json(contacts);
});

// GET all Contacs
router.get('/:id', async (req, res) => {
  const contacts = await Contact.findById(req.params.id);
  res.json(contacts);
});

// ADD a new task
router.post('/', async (req, res) => {
  const { nombre, email, telefono, tipocontact} = req.body;
  const task = new Contact({nombre, email,telefono,tipocontact});
  await task.save();
  res.json({status: 'Contacs Saved'});
});

// UPDATE a new task
router.put('/:id', async (req, res) => {
  const { nombre, email,telefono,tipocontact  } = req.body;
  const newContact = {nombre, email,telefono,tipocontact};
  await Contact.findByIdAndUpdate(req.params.id, newContact);
  res.json({status: 'Task Updated'});
});

router.delete('/:id', async (req, res) => {
  await Contact.findByIdAndRemove(req.params.id);
  res.json({status: 'Task Deleted'});
});

module.exports = router;
