/** @format */

import React, { Component } from "react";

class App extends Component {
  constructor() {
    super();
    this.state = {
      nombre: "",
      email: "",
      telefono: "",
      tipocontact: "",
      _id: "",
      contacts: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.addContact = this.addContact.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  addContact(e) {
    e.preventDefault();
    if (this.state._id) {
      fetch(`/api/contacts/${this.state._id}`, {
        method: "PUT",
        body: JSON.stringify({
          nombre: this.state.nombre,
          email: this.state.email,
          telefono: this.state.telefono,
          tipocontact: this.state.tipocontact,
        }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          // window.M.toast({ html: "Contact Updated" });
          this.setState({
            _id: "",
            nombre: "",
            email: "",
            telefono: "",
            tipocontact: "",
          });
          this.fetchContact();
        });
    } else {
      if (this.state.tipocontact === "" || this.state.tipocontact == null) {
        this.state.tipocontact = document.getElementById("tipocontac").value;
      }
        
      fetch("/api/contacts", {
        method: "POST",
        body: JSON.stringify(this.state),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          // window.M.toast({ html: "Task Saved" });
          alert("Guardado");
          this.setState({
            nombre: "",
            email: "",
            telefono: "",
            tipocontact: "",
          });
          this.fetchContact();
        })
        .catch((err) => console.error(err));
    }
  }

  deleteContact(id) {
    if (confirm("Are you sure you want to delete it?")) {
      fetch(`/api/contacts/${id}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          // M.toast({ html: "Contacs deleted" });
          this.fetchContact();
        });
    }
  }

  editContact(id) {
    fetch(`/api/contacts/${id}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.setState({
          nombre: data.nombre,
          email: data.email,
          telefono: data.telefono,
          tipocontact: data.tipocontact,
          _id: data._id,
        });
      });
  }

  componentDidMount() {
    this.fetchContact();
  }

  fetchContact() {
    fetch("/api/contacts")
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          contacts: data,
        });
        console.log(this.state.contacts);
      });
  }

  render() {
    return (
      <div>
        {/* NAVIGATION */}
        <div className="jumbotron jumbotron-fluid" style={{}}>
          <div className="container">
            <h1 className="display-4">CRUD MERN</h1>
            <p className="lead">DANIEL CAQUI</p>
          </div>
        </div>
        <div className="container p-5">
          <div className="row">
            <div className="col-sm-4">
              <div className="card animated flipInY">
                <div class="card-header bg-dark text-white text-center">
                  <h3>Agregar Contacto</h3>
                </div>
                <div className="card-body">
                  <form onSubmit={this.addContact}>
                    <div className="form-group">
                      <input
                        className="form-control"
                        name="nombre"
                        onChange={this.handleChange}
                        value={this.state.nombre}
                        type="text"
                        placeholder="Name"
                        autoFocus
                      />
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control"
                        name="email"
                        onChange={this.handleChange}
                        value={this.state.email}
                        type="text"
                        placeholder="E-mail"
                        autoFocus
                      />
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control"
                        name="telefono"
                        onChange={this.handleChange}
                        value={this.state.telefono}
                        type="text"
                        placeholder="Phone"
                      />
                    </div>
                   
                        <label class="radio-inline">
                      <input
                        id="tipocontac"
                        className=""
                        name="tipocontact"
                        type="radio"
                        onChange={this.handleChange}
                        value="Personal"
                      /> Personal   . </label>   
                   
                    
                      <label class="radio-inline">
                      <input
                        id="tipocontac"
                        className=" "
                        name="tipocontact"
                        type="radio"
                        onChange={this.handleChange}
                        value="Profesional"
                      /> Profesional </label>
                      
                    

                    <button
                      className="form-control"
                      type="submit"
                      className="btn btn-primary btn-block"
                    >
                      Enviar
                    </button>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <table className="table table-striped ">
                <thead className="thead-dark">
                  <tr>
                    <th> Nombre </th> <th> Email </th> <th>Telefono </th>{" "}
                    <th>Tipo de Contacto </th> <th>Acción</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.contacts.map((contact) => {
                    return (
                      <tr key={contact._id}>
                        <td> {contact.nombre} </td> <td> {contact.email} </td>{" "}
                        <td> {contact.telefono} </td>{" "}
                        <td> {contact.tipocontact} </td>
                        <td>
                          <button
                            className="btn btn-danger btn-sm"
                            onClick={() => this.deleteContact(contact._id)}
                            style={{
                              margin: "2px",
                            }}
                          >
                            <i className="fa fa-trash"></i>
                          </button>
                          <button
                            className="btn btn-success btn-sm"
                            onClick={() => this.editContact(contact._id)}
                            style={{
                              margin: "2px",
                            }}
                          >
                            <i className="fa fa-edit"></i>
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
